package com.tnk.db.contracts;

import android.provider.BaseColumns;

/**
 * Created by Tom on 2019-02-17.
 *
 *
 *
 */

public final class Contract_Project {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private Contract_Project() {

    }

    /* Inner Class that defines the table contents */
    public static class ProjectEntry implements BaseColumns {
        public static final String TABLE_NAME = "projects" ;
        public static final String COLUMN_ABL = "abl";
        public static final String COLUMN_TITLE_SHORT = "titleShort";
        public static final String COLUMN_TITLE_LONG = "titleLong";
        public static final String COLUMN_DATESTART = "startdate";
        public static final String COLUMN_DATEEND = "enddate";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_TAGS = "tags";
        public static final String COLUMN_LEAD = "lead";
        public static final String COLUMN_ELEC = "elec";
        public static final String COLUMN_MECH = "mech";
        public static final String COLUMN_PROGRESS = "progress";
        public static final String COLUMN_CUSTOMER = "customer";
        public static final String COLUMN_NOTES = "notes";

        // for iterating through populators
        public static final String[] COLUMNS = {
                _ID,
                COLUMN_ABL,
                COLUMN_TITLE_SHORT,
                COLUMN_TITLE_LONG,
                COLUMN_DATESTART,
                COLUMN_DATEEND,
                COLUMN_STATUS,
                COLUMN_TAGS,
                COLUMN_LEAD,
                COLUMN_ELEC,
                COLUMN_MECH,
                COLUMN_PROGRESS,
                COLUMN_CUSTOMER,
                COLUMN_NOTES
        };

        // for counting purposes?
        public static final int INT_ID = 0;
        public static final int INT_ABL = 1;
        public static final int INT_TITLE_SHORT = 2;
        public static final int INT_TITLE_LONG = 3;
        public static final int INT_DATESTART = 4;
        public static final int INT_DATEEND = 5;
        public static final int INT_STATUS = 6;
        public static final int INT_TAGS = 7;
        public static final int INT_LEAD = 8;
        public static final int INT_ELEC = 9;
        public static final int INT_MECH = 10;
        public static final int INT_PROGRESS = 11;
        public static final int INT_CUSTOMER = 12;
        public static final int INT_NOTES = 13;
    }
}
