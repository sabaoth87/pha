package com.tnk.pha.issues;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tnk.R;
import com.tnk.db.contracts.Contract_Issue;
import com.tnk.db.helpers.DbHelper_Issues;
import com.tnk.db.IssueCursorAdapter;
import com.tnk.pha.adapters.PHA_ELA_Issues;
import com.tnk.pha.util.Variables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PHA_Issue_List extends AppCompatActivity {

    private static final String TAG = "PHA:IssueList";
    private static final int ACTIVITY_CREATE = 0;
    private static final int ACTIVITY_EDIT = 1;
    public String[] lvIds = {};
    public ListView lvIssues;
    public TextView tvIssues;
    private ExpandableListView expListView;
    private Toolbar toolbar;
    private DbHelper_Issues dbHelper_issues;
    private SQLiteDatabase db;

    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pha_issues_list);

        toolbar = findViewById(R.id.tb_pha_issues);
        expListView = findViewById(R.id.elv_issues);
        tvIssues = findViewById(R.id.tv_issues);

        // Try to 'make' the db and table
        Context context = getApplicationContext();
        dbHelper_issues = new DbHelper_Issues(context);
        db = dbHelper_issues.getWritableDatabase();
        dbHelper_issues.close();

        boolean table_ready = dbHelper_issues.checkTable(db);

        if (!table_ready){
            boolean table_created = dbHelper_issues.create_table(context);
        }

        setSupportActionBar(toolbar);
        //registerForContextMenu(lvIssues);

        AdapterView.OnItemClickListener mMessageClickHandler = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                Context context = getApplicationContext();
                Log.v(TAG, "<INFO>    Sending i: " + lvIds[i]);
                Intent editIntent = new Intent(context, PHA_Issue_Entry.class);
                editIntent.putExtra(Contract_Issue.IssueEntry._ID, lvIds[i]);
                startActivityForResult(editIntent, ACTIVITY_EDIT);
            }
        };

        //lvIssues.setOnItemClickListener(mMessageClickHandler);

        registerForContextMenu(expListView);

        // TODO testing the loading of data after the startActivityForResult concludes
        // loadELVdata();




        //call fill data after the LV and other objects have
        //been instantiated
        fillData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pha_issue_list, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_lp_issue_list, menu);
    }

    private void createIssue() {
        Intent j = new Intent(this, PHA_Issue_Entry.class);
        startActivityForResult(j, ACTIVITY_CREATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        //this is for reloading the list
        fillData();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Context context = getApplicationContext();
        switch (item.getItemId()) {
            case R.id.menu_lp_issue_delete:
                //delete the selected issue
                if (Variables.mode_debug) {Log.v(TAG, "Attempting to delete issue");}
                AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
                //phaDbHlpr.deleteReminder(info.id);
                    /*
                    @TODO 10 - Ensure the _id is properly fetched
                    Get the 'edit' option working first
                     */

                    Toast.makeText(context,
                            "MenuItem.Id " + item.getItemId()
                                    + " MenuItem.toString " + item.toString(),
                            Toast.LENGTH_SHORT).show();
                fillData();
                return true;
            case R.id.menu_lp_issue_edit:
                //edit the selected issue
                Toast.makeText(context,
                        "MenuItem.Id " + item.getItemId()
                                + " MenuItem.toString " + item.toString(),
                        Toast.LENGTH_SHORT).show();
        }
        return super.onContextItemSelected(item);
    }

    private void fillData() {
        Context context = getApplicationContext();
        DbHelper_Issues dbHelper = new DbHelper_Issues(context);
        Cursor queryResult = dbHelper.findAllIssues();
        Log.v(TAG, "queryResult = " + queryResult.getCount());
        int totalList = queryResult.getCount();
        int count = 0;
        lvIds = new String[queryResult.getCount()];
        while (queryResult.moveToNext()) {
            int index;
            Log.v(TAG, "Query Loop " + count + "/" + totalList);
            index = queryResult.getColumnIndexOrThrow(Contract_Issue.IssueEntry._ID);
            String entryId = queryResult.getString(index);
            lvIds[count] = entryId;
            count++;
        }


        //Setup Cursor adapter using the Cursor from the last step
        IssueCursorAdapter issueAdapter = new IssueCursorAdapter(this, queryResult);
        //Attach Cursor adapter to the ListView


        //tvIssues.setText("Count =" + queryResult.getCount() + " " + lvIds.toString());
        dbHelper.close();
        loadELVdata();
    }

    private void loadELVdata() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        Context context = getApplicationContext();
        DbHelper_Issues dbHelper = new DbHelper_Issues(context);
        Cursor queryResult = dbHelper.findAllIssues();
        Log.v(TAG, "queryResult = " + queryResult.getCount());
        int totalList = queryResult.getCount();
        int count = 0;
        lvIds = new String[queryResult.getCount()];
        while (queryResult.moveToNext()) {
            int index;
            Log.v(TAG, "Query Loop " + count + "/" + totalList);
            index = queryResult.getColumnIndexOrThrow(Contract_Issue.IssueEntry._ID);
            listDataHeader.add(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Issue.IssueEntry.COLUMN_PROJECT)));
            String childTitle = new String(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Issue.IssueEntry.COLUMN_PROJECT))+"children");
            List<String> children = new ArrayList<>();
            children.add(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Issue.IssueEntry.COLUMN_TITLE)));
            listDataChild.put(listDataHeader.get(count), children);
            String entryId = queryResult.getString(index);
            lvIds[count] = entryId;
            count++;
        }

        // In case there are no entries yet, show the user an example
        //  TODO Make this a default entry in the Db somehow
        if (listDataHeader==null) {
            listDataHeader.add("Example Project");
            List<String> mp_monday = new ArrayList<String>();
            mp_monday.add("This is an example issue entry...");

            listDataChild.put(listDataHeader.get(0), mp_monday);

        }

        // @FIX_ME Expand this ELA to include ALL of the children
        // Need to pass all fields that should be populated on the child
        listAdapter = new PHA_ELA_Issues(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
    }

    /*private AdapterView.OnItemClickListener mMessageClickHandler = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
            Context context = getApplicationContext();
            Log.v(TAG, "<INFO>    Sending i: " + lvIds[i]);
            Intent editIntent = new Intent(context, PHA_Issue_Entry.class);
            editIntent.putExtra(Contract_Issue.IssueEntry._ID, lvIds[i]);
            startActivityForResult(editIntent, ACTIVITY_EDIT);
        }
    };*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case R.id.menu_issue_insert:
                //User chose the "+Reminder" item, show the app settings UI...
                createIssue();
                return true;
            case R.id.menu_issue_list:
                return true;
            case R.id.menu_issue_settings:
                return true;

            default:
                // If we got here, the user's actions was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

}