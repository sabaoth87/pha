package com.tnk.db;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.tnk.R;
import com.tnk.db.contracts.Contract_Issue;
import com.tnk.db.contracts.Contract_Project;

/**
 * Created by Tom on 2019-02-17.
 *
 *
 *
 */

public class ProjectCursorAdapter extends CursorAdapter {
    public ProjectCursorAdapter(Context context, Cursor cursor) { super(context, cursor, 0  );}

    // the newView method is used to inflate a new view and return it,
    // you do not bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_reminders, parent, false);
    }

    // the bindView method is used to bind all data to a given view
    // such as setting the text on a text view
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        /*
        Find fields to populate in inflated template
         */
        TextView tvId = view.findViewById(R.id.item_tv_id);
        TextView tvABL = (TextView) view.findViewById(R.id.item_tv_abl);
        TextView tvShort = (TextView) view.findViewById(R.id.item_tv_shortTitle);
        //TextView tvLong = (TextView) view.findViewById(R.id.item_tv_longTitle);
        TextView tvDateStart = (TextView) view.findViewById(R.id.item_tv_dateStart);
        //TextView tvDateEnd = (TextView) view.findViewById(R.id.dateEnd);
        TextView tvStatus = (TextView) view.findViewById(R.id.item_tv_status);
        TextView tvTags = (TextView) view.findViewById(R.id.item_tv_tags);
        //TextView tvLead = (TextView) view.findViewById(R.id.item_tv_lead);
        //TextView tvElec = (TextView) view.findViewById(R.id.item_tv_elec);
        //TextView tvMech = (TextView) view.findViewById(R.id.item_tv_mech);
        TextView tvProgress = (TextView) view.findViewById(R.id.item_tv_progress);
        //TextView tvCustomer = (TextView) view.findViewById(R.id.item_tv_customer);
        TextView tvNotes = (TextView) view.findViewById(R.id.item_tv_notes);

        /*
        Extract properties from cursor
         */
        String id = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry._ID));
        String abl = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_ABL));
        String titleShort = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_TITLE_SHORT));
        String titleLong = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_TITLE_LONG));
        String dateStart = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_DATESTART));
        String dateEnd = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_DATEEND));
        String status = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_STATUS));
        String tags = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_TAGS));
        String leadAssoc = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_LEAD));
        String elecAssoc = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_ELEC));
        String mechAssoc = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_MECH));
        String progress = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_PROGRESS));
        String customer = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_CUSTOMER));
        String notes = cursor.getString(cursor.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_NOTES));

        /*
        Populate fields with extracted properties
         */
        tvId.setText(id);
        tvABL.setText(abl);
        tvShort.setText(titleShort);
        //tvLong.setText(titleLong);
        tvDateStart.setText(dateStart);
        //tvDateEnd.setText(dateEnd);
        tvStatus.setText(status);
        tvTags.setText(tags);
        //tvLead.setText(leadAssoc);
        //tvElec.setText(elecAssoc);
        //tvMech.setText(mechAssoc);
        tvProgress.setText(progress);
        //tvCustomer.setText(customer);
        tvNotes.setText(notes);
    }
}
