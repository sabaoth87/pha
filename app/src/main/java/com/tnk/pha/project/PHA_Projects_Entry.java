package com.tnk.pha.project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tnk.R;
import com.tnk.db.contracts.Contract_Project;
import com.tnk.db.dbConstants;
import com.tnk.db.helpers.DbHelper_Projects;
import com.tnk.db.items.Item_Project;
import com.tnk.pha.util.Variables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class PHA_Projects_Entry extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "PHA_Projects Entry :: ";

    /*
    Shared Vars
     */
    private Bundle extras;
    private boolean incomingProject = false;
    private boolean beginEntrySet = false;
    static final String DATE_TIME_FORMAT = "yyyy-MM-dd kk:mm:ss";
    private Long RowId;
    private DbHelper_Projects PHA_dbHelper;
    private String newString;

    /*
    Shared Vars
    Module-Specific Naming
     */
    private Toolbar toolbar;

    /*
    Module Specific Vars
     */
    private EditText et_abl, et_short, et_long, et_datestart, et_dateend, et_status, et_tags,
    et_lead, et_elec, et_mech, et_progress, et_customer, et_notes;
    private ProgressBar pb_project, pb_time;

    private FloatingActionButton fab_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_card);
        toolbar = findViewById(R.id.tb_project_view);
        setSupportActionBar(toolbar);

        RowId = savedInstanceState != null ? savedInstanceState.getLong(Contract_Project.ProjectEntry._ID) : null;
        PHA_dbHelper = new DbHelper_Projects(this);

        et_abl = findViewById(R.id.card_project_abl);
        et_short = findViewById(R.id.card_project_short);
        et_long = findViewById(R.id.card_project_long);
        et_datestart = findViewById(R.id.card_project_date_start);
        et_dateend = findViewById(R.id.card_project_date_end);
        et_status = findViewById(R.id.card_project_status);
        // TODO Implement in the future!!
        //et_tags = findViewById(R.id.card_project_tags);
        et_lead = findViewById(R.id.card_project_lead);
        et_elec =findViewById(R.id.card_project_elec);
        et_mech = findViewById(R.id.card_project_mech);
        et_progress = findViewById(R.id.card_project_progress);
        et_customer = findViewById(R.id.card_project_customer);
        et_notes = findViewById(R.id.card_project_notes);

        pb_project = (ProgressBar) findViewById(R.id.card_project_progressbar);
        pb_time = (ProgressBar) findViewById(R.id.card_project_time_progressbar);

        fab_add = (FloatingActionButton) findViewById(R.id.card_fab_add);
        fab_add.bringToFront();
        fab_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
            /*
            TODO - Populate the Toolbar
             */
                    case (R.id.card_fab_add):
                        saveState();
                        setResult(RESULT_OK);
                        Snackbar.make(v, getString(R.string.saveConfirm), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        finish();
                        break;
                }
            }
        });


        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                /* Not too sure what to add here for Projects yet...*/
            } else {
                Log.v(TAG, "Setting intent values");
                // TODO Investigate this in PHA_Reminders for implementation
                //findEntryForEdit(extras.getString(Contract_Project.ProjectEntry._ID));
                incomingProject = true;
                Cursor cProject = findEntryForEdit(extras.getString(Contract_Project.ProjectEntry._ID));
                if (cProject != null) {
                    Log.v(TAG, "<Success> Db entry location valid! /n Returning information");
                    loadProjectValues(cProject);
                }
                Log.v(TAG, "<Error> Issue Cursor was invalid!");
            }
        } else {
                Log.v(TAG, "Setting default values");
            newString = (String) savedInstanceState.getSerializable("want");

            // TODO Fill this will default value population!

            }

        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            int rowId = extras != null ? extras.getInt("RowId") : -1;
            //Do things with the RowID here
        }

    }

    private void saveState() {
        Log.v(TAG, ":: Saving State");
        String abl = et_abl.getText().toString();
        String shortTitle = et_short.getText().toString();
        String longTitle = et_long.getText().toString();
        String dateStart = et_datestart.getText().toString();
        String dateEnd = et_dateend.getText().toString();
        String status = et_status.getText().toString();
        //String tags = et_tags.getText().toString();
        String lead = et_lead.getText().toString();
        String elec = et_elec.getText().toString();
        String mech = et_mech.getText().toString();
        String progress = et_progress.getText().toString();
        String customer = et_customer.getText().toString();
        String notes = et_notes.getText().toString();

        Context context = getApplicationContext();
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATE_TIME_FORMAT);

        Item_Project newProject = new Item_Project();
        newProject.setProjectABL(abl);
        newProject.setProjectShort(shortTitle);
        newProject.setProjectLong(longTitle);
        newProject.setProjectDateStart(dateStart);
        newProject.setProjectDateEnd(dateEnd);
        newProject.setProjectStatus(status);
        //newProject.setProjectTags(tags);
        newProject.setProjectAssocLead(lead);
        newProject.setProjectAssocElec(elec);
        newProject.setProjectAssocMech(mech);
        newProject.setProjectProgress(Long.valueOf(progress));
        newProject.setProjectCustomer(customer);
        newProject.setProjectNotes(notes);

        if (RowId == null) {
            Log.v(TAG, "No current RowId, adding new project to Db...");

            long id = PHA_dbHelper.addProject(newProject);
            Log.v(TAG, "Added entry " + id);

            if (id > 0) {
                RowId = id;
            }
        } else {
            newProject.setID(Math.toIntExact(RowId));
            Log.v(TAG, "Updating entry _ID " + newProject.getID());
            Toast.makeText(context, "Updating", Toast.LENGTH_LONG).show();
            PHA_dbHelper.updateProjectById(newProject);
        }

    }

    private void setRowIdFromIntent() {
        if (RowId == null) {
            Bundle extras = getIntent().getExtras();

            RowId = extras != null ? extras.getLong(Contract_Project.ProjectEntry._ID) : null;
            Toast.makeText(getApplicationContext(), "An _ID was sent to me! + " + RowId, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        PHA_dbHelper.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Create a menu for Projects
        getMenuInflater().inflate(R.menu.tb_menu_prj_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_item_prj_db){
            // Do something
            Snackbar.make(this.getCurrentFocus(), "Db Actions Coming Soon", Snackbar.LENGTH_LONG)
                                    .setAction ("db", null).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setRowIdFromIntent();
        populateFields();
    }

    private void populateFields() {

        if (RowId != null) {

        } else if (incomingProject) {
            /*
            This is the area to populate fields that is generated from the Intent package
             */
            et_abl.setText(newString);
        } else {

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String defaultABLKey = "deafault_project_abl";
            String defaultABL = prefs.getString(defaultABLKey, "");
            // TODO Establish the preferences...
            if ("".equals(defaultABL) == false)
                et_abl.setText(defaultABL);

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
         /*
         @FIX_ME __ - onSaveInstanceState
         Attempting to stash the RowId of the current RowId, so we can try to persist
          */
        outState.putLong(Contract_Project.ProjectEntry._ID, RowId);
    }

    public void loadProjectValues(Cursor project) {
        //load the Project information into the UI here
        //fab_add.setEnabled(false);

        if (Variables.mode_debug){Log.v(TAG,"Trying to load Editable Project...");}
        if (project==null) {
            if (Variables.mode_debug){Log.v(TAG,"Cursor with the Editable Project was found to be null!");}
            et_abl.setText(R.string.et_entry_null);
            et_short.setText(R.string.et_entry_null_body);
            et_long.setText(R.string.et_entry_null_body);

            beginEntrySet = false;
        }
        else {
            if (Variables.mode_debug){Log.v(TAG,"Cursor is valid! Populating UI");}

            while(project.moveToNext()) {
                int index;
                // how many columns to cycle through?
                int columnsLength = Contract_Project.ProjectEntry.COLUMNS.length;
                String[] outputString = new String[columnsLength];

                int count = 0;

                for (String s: Contract_Project.ProjectEntry.COLUMNS) {

                    index  = project.getColumnIndexOrThrow(s);
                    outputString[count] = project.getString(index);
                    count++;

                }

                index = project.getColumnIndexOrThrow(Contract_Project.ProjectEntry._ID);
                RowId = project.getLong(index);

                Log.v(TAG, "Editable Reminder entry ID: " + outputString[Contract_Project.ProjectEntry.INT_ID]);

                et_abl.setText(outputString[Contract_Project.ProjectEntry.INT_ABL]);
                et_short.setText(outputString[Contract_Project.ProjectEntry.INT_TITLE_SHORT]);
                et_long.setText(outputString[Contract_Project.ProjectEntry.INT_TITLE_LONG]);
                et_datestart.setText(outputString[Contract_Project.ProjectEntry.INT_DATESTART]);
                et_dateend.setText(outputString[Contract_Project.ProjectEntry.INT_DATEEND]);
                et_status.setText(outputString[Contract_Project.ProjectEntry.INT_STATUS]);
                //et_tags.setText(outputString[Contract_Project.ProjectEntry.INT_TAGS]);
                et_lead.setText(outputString[Contract_Project.ProjectEntry.INT_LEAD]);
                et_elec.setText(outputString[Contract_Project.ProjectEntry.INT_ELEC]);
                et_mech.setText(outputString[Contract_Project.ProjectEntry.INT_MECH]);
                et_progress.setText(outputString[Contract_Project.ProjectEntry.INT_PROGRESS]);
                et_customer.setText(outputString[Contract_Project.ProjectEntry.INT_CUSTOMER]);
                et_notes.setText(outputString[Contract_Project.ProjectEntry.INT_NOTES]);

                /*
                I split this activity into two, one for edit, one for view
                this should no longer be required
                et_abl.setEnabled(false);
                et_short.setEnabled(false);
                et_long.setEnabled(false);
                et_datestart.setEnabled(false);
                et_dateend.setEnabled(false);
                et_status.setEnabled(false);
                //et_tags.setEnabled(false);
                et_lead.setEnabled(false);
                et_elec.setEnabled(false);
                et_mech.setEnabled(false);
                et_progress.setEnabled(false);
                et_customer.setEnabled(false);
                et_notes.setEnabled(false);
                */
                pb_project.setMax(100);
                //pb_project.setMin(0);
                pb_project.setProgress(0);
                pb_project.setIndeterminate(false);
                pb_project.setProgress(Math.toIntExact(Integer.valueOf(outputString[Contract_Project.ProjectEntry.INT_PROGRESS])));
                beginEntrySet = true;
                pb_time.setMax(100);
                //pb_time.setMin(0);
                pb_time.setProgress(Math.toIntExact(Integer.valueOf(outputString[Contract_Project.ProjectEntry.INT_PROGRESS])));
            }

            beginEntrySet = false;
        }
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            /*
            TODO - Populate the Toolbar
             */
            case (R.id.card_fab_add):
                saveState();
                setResult(RESULT_OK);
                Snackbar.make(arg0, getString(R.string.saveConfirm), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                finish();
                break;
        }
    }

    public Cursor findEntryForEdit(String id) {
        Context context = getApplicationContext();
        DbHelper_Projects dbHelper = new DbHelper_Projects(context);

        Cursor forEdit = dbHelper.findProject(dbConstants.FINDBY_ID, id);
        return forEdit;
    }

    /*
    @REMARK 00 - Date/Time Format
    This is probably not required here
    I should incorporate this into the entry values themselves
    So just   <title>, <body>, <timeDate> are the variables for the object
    format/convert using Calendar or android.text.format.DateUtils.formatDateTime
    */
    public String formatDateTime(String timeToFormat) {

        Context context = getApplicationContext();

        String finalDateTime = "";

        SimpleDateFormat iso8601Format = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        Date date = null;
        if (timeToFormat !=null){
            try {
                date = iso8601Format.parse(timeToFormat);
            } catch (ParseException e){
                date = null;
            }

            if (date != null) {
                long when = date.getTime();
                int flags = 0;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_TIME;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_DATE;
                flags |= android.text.format.DateUtils.FORMAT_ABBREV_MONTH;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_YEAR;

                finalDateTime = android.text.format.DateUtils.formatDateTime(context,
                        when + TimeZone.getDefault().getOffset(when), flags);
            }
        }
        return finalDateTime;
    }

}
