package com.tnk.pha.project;

/**
 * Created by Tom on 2019-02-17.
 *
 *
 *
 */

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewOverlay;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.tnk.R;
import com.tnk.db.contracts.Contract_Project;
import com.tnk.db.dbConstants;
import com.tnk.db.helpers.DbHelper_Projects;
import com.tnk.db.items.Item_Project;
import com.tnk.pha.adapters.ELA_Projects;
import com.tnk.pha.adapters.ProjectsGridAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public  class PHA_Projects extends AppCompatActivity {


    private static String TAG = "PHA[Projects]";
    private static final int ACTIVITY_CREATE = 0;
    private static final int ACTIVITY_EDIT = 1;

    private ExpandableListAdapter listAdapter_default;
    private Toolbar toolbar;
    private DbHelper_Projects dbHelper;
    private SQLiteDatabase sqlDb;
    private ViewOverlay overlay;
    private GridView projectCards;
    private ProjectsGridAdapter prjGridAdapter;

    private List<String> listDataHeader_default;
    private List<Item_Project> project_details;
    private HashMap<String, List<String>> listDataChild_default;

    public String[] gridIds = {};


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pha_projects);
        toolbar = findViewById(R.id.tb_PHA_Projects);
        setSupportActionBar(toolbar);

        Button btn_PRJ_add = findViewById(R.id.btn_projects_add);
        Button btn_PRJ_clear = findViewById(R.id.btn_projects_clear);
        final Spinner sp_PRJ_assoc = findViewById(R.id.sp_projects_associate);
        final Spinner sp_PRJ_loc = findViewById(R.id.sp_projects_locations);


        // Try to 'make' the db and table
        Context context = getApplicationContext();
        dbHelper = new DbHelper_Projects(context);
        sqlDb = dbHelper.getWritableDatabase();
        //dbHelper.close();

        boolean table_ready = dbHelper.checkTable(sqlDb);

        if (!table_ready){
            boolean table_created = dbHelper.create_table(context);
            if(table_created)
                Toast.makeText(PHA_Projects.this,
                        "New Table Has Been Created",
                        Toast.LENGTH_LONG).show();
        }

        // <Grid> Start

        project_details = getListData();
        projectCards = (GridView) findViewById(R.id.grid_project_cards);
        prjGridAdapter = new ProjectsGridAdapter(this, project_details);
        projectCards.setAdapter(prjGridAdapter);
        registerForContextMenu(projectCards);
        projectCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = projectCards.getItemAtPosition(position);

                Item_Project project = (Item_Project) o;
                Toast.makeText(PHA_Projects.this, "Selected :"
                + " " + project, Toast.LENGTH_LONG).show();

                Context context = getApplicationContext();
                Log.v(TAG, "<INFO>    Sending i: " + gridIds[position]);
                Intent editIntent = new Intent(context, PHA_Project_View.class);
                editIntent.putExtra(Contract_Project.ProjectEntry._ID, gridIds[position]);
                startActivityForResult(editIntent, ACTIVITY_EDIT);
            }
        });

        // <Grid> END

        ExpandableListView expListView = findViewById(R.id.elv_projects);


        loadELVdata();

        //A list of heads and a list of children needs to be build from the SQL Db
        listAdapter_default = new ELA_Projects(this, listDataHeader_default, listDataChild_default);

        // setting list adapter
        expListView.setAdapter(listAdapter_default);

        btn_PRJ_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Log.v(TAG, "ATTEMPT: Add Project");
                createProject();
            }
        });

        btn_PRJ_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "ATTEMPT: Clear Fields!");
                //clearInputs();
            }
        });



        //                          FOR MY SPINNERS
        //Create and ArrayAdapter using the string array and default layout
        final ArrayAdapter<CharSequence> adap_assoc = ArrayAdapter.createFromResource(this,
                R.array.associates, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adap_loc = ArrayAdapter.createFromResource(this,
                R.array.servings, android.R.layout.simple_spinner_item);
        //Specify the layout to use when the list of choices appears
        adap_assoc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adap_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Apply the adapter to the spinner
        sp_PRJ_assoc.setAdapter(adap_assoc);
        sp_PRJ_loc.setAdapter(adap_loc);
        sp_PRJ_loc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String spinnerValue = sp_PRJ_loc.getSelectedItem().toString();

                if (spinnerValue.equals("Tim B.")) {
                    sp_PRJ_loc.setAdapter(adap_assoc);
                }else if (spinnerValue.equals("Tom K.")){
                    sp_PRJ_loc.setAdapter(adap_assoc);
                }else if (spinnerValue.equals("Dan B.")){
                    sp_PRJ_loc.setAdapter(adap_assoc);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.list_menu_item_longpress, menu);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pha__reminder__list, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.lp_menu_delete:
                //delete the selected task
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                /*
                // TODO Testing Deletion
                */

                Log.v(TAG, "About to delete " + gridIds[info.position]);
                Snackbar.make(this.getCurrentFocus(), "Deleting " + gridIds[info.position], Snackbar.LENGTH_LONG).setAction("Action", null).show();
                dbHelper.deleteProjectById(gridIds[info.position]);
                //fillData();
                // update after the db change
                project_details = getListData();
                // attempt to draw the gridview after the dataset changes
                //projectCards.invalidateViews();
                prjGridAdapter.notifyDataSetChanged();
                prjGridAdapter = new ProjectsGridAdapter(this, project_details);
                projectCards.setAdapter(prjGridAdapter);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void createProject() {
        Intent j = new Intent(this, PHA_Projects_Entry.class);
        startActivityForResult(j, ACTIVITY_CREATE);
    }

    private List<Item_Project> getListData() {
        Context context = getApplicationContext();
        Cursor queryResult = dbHelper.findProject(dbConstants.FINDALL, null);
        Log.v(TAG, "queryResult = " + queryResult.getCount());

        int totalList = queryResult.getCount();
        int count = 0;
        List<Item_Project> gridList = new ArrayList<Item_Project>();
        gridIds = new String[queryResult.getCount()];
        Item_Project[] projectArray = new Item_Project[queryResult.getCount()];

        // Populate the GridView information here!
        while (queryResult.moveToNext()){
            int index = -1;
            Item_Project queryProject = new Item_Project();

            Log.v(TAG, "Query Loop " + (count+1) + "/" + totalList);

            index = queryResult.getColumnIndexOrThrow(Contract_Project.ProjectEntry._ID);
            queryProject.setID(index);
            Log.v(TAG, "_ID :" + index);

            String progress = new String(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_PROGRESS)));
            Log.v(TAG, "Progress :" + progress);

            if(progress == null) {
                queryProject.setProjectProgress(0);

            } else {
                queryProject.setProjectProgress(Long.parseLong(progress));

            }

            //String shortTitle = new String(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_TITLE_SHORT)));
            //String abl = new String(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_ABL)));

            queryProject.setProjectABL(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_ABL)));
            queryProject.setProjectShort(queryResult.getString(queryResult.getColumnIndexOrThrow(Contract_Project.ProjectEntry.COLUMN_TITLE_SHORT)));


            String entryId = queryResult.getString(index);

            gridList.add(queryProject);
            projectArray[count] = queryProject;

            gridIds[count] = entryId;
            count++;
        }

        return gridList;
    }

    private void loadELVdata() {

        listDataHeader_default = new ArrayList<String>();
        listDataChild_default = new HashMap<String, List<String>>();

        // Default Header - Business Areas
        listDataHeader_default.add("DEMI");
        listDataHeader_default.add("IVORY");
        listDataHeader_default.add("ULTRA");
        listDataHeader_default.add("THICK");
        listDataHeader_default.add("LINERS");
        listDataHeader_default.add("UTIL");
        listDataHeader_default.add("FAM");
        listDataHeader_default.add("Office");

        // DEMI
        List<String> lines_demi = new ArrayList<String>();
        lines_demi.add("LM - L58");
        lines_demi.add("LM - L59");
        lines_demi.add("LM - L60");
        lines_demi.add("H - L80");
        lines_demi.add("DXL - L81");


        // IVORY
        List<String> lines_ivory = new ArrayList<String>();
        lines_ivory.add("L62");
        lines_ivory.add("L67");
        lines_ivory.add("L68");
        lines_ivory.add("L92");

        // ULTRA
        List<String> lines_ultra = new ArrayList<String>();
        lines_ultra.add("L38");
        lines_ultra.add("L63");
        lines_ultra.add("L64");
        lines_ultra.add("L65");
        lines_ultra.add("L66");

        // THICK
        List<String> lines_think = new ArrayList<String>();
        lines_think.add("L42");
        lines_think.add("L32");
        lines_think.add("L33");
        lines_think.add("L34");

        // LINERS
        List<String> lines_liners = new ArrayList<String>();
        lines_liners.add("L74");
        lines_liners.add("L75");
        lines_liners.add("L77");
        lines_liners.add("L78");

        // UTIL
        List<String> lines_util = new ArrayList<String>();
        lines_util.add("Demi Pub");
        lines_util.add("Ultra Pub");
        lines_util.add("Thick Pub");

        // FAM
        List<String> lines_fam = new ArrayList<String>();
        lines_fam.add("Oven");

        // Office
        List<String> lines_office = new ArrayList<String>();
        lines_office.add("null");

        listDataChild_default.put(listDataHeader_default.get(0), lines_demi);
        listDataChild_default.put(listDataHeader_default.get(1), lines_ivory);
        listDataChild_default.put(listDataHeader_default.get(2), lines_ultra);
        listDataChild_default.put(listDataHeader_default.get(3), lines_think);
        listDataChild_default.put(listDataHeader_default.get(4), lines_liners);
        listDataChild_default.put(listDataHeader_default.get(5), lines_util);
        listDataChild_default.put(listDataHeader_default.get(6), lines_fam);
        listDataChild_default.put(listDataHeader_default.get(7), lines_office);


    }


    @Override
    protected void onResume() {
        super.onResume();
        // update after the db change
        project_details = getListData();
        // attempt to draw the gridview after the dataset changes
        prjGridAdapter.notifyDataSetChanged();
        //projectCards.invalidateViews();
        prjGridAdapter = new ProjectsGridAdapter(this, project_details);
        projectCards.setAdapter(prjGridAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        dbHelper.close();
    }

}
