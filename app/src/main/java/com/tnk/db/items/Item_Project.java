package com.tnk.db.items;

import android.util.Log;

import com.tnk.db.contracts.Contract_Project;

/**
 * Created by Tom on 2019-02-17.
 *
 *
 *
 */

public class Item_Project {

    // fields
    private int projectID;
    private String projectABL;
    private String projectTitleShort;
    private String projectTitleLong;
    private String projectDateStart;
    private String projectDateEnd;
    private String projectStatus;
    private String projectTags;
    private String projectAssocLead;
    private String projectAssocElec;
    private String projectAssocMech;
    private long projectProgress;
    private String projectCustomer;
    private String projectNotes;

    // constructors
    public Item_Project() {
    }


    public Item_Project(int id,
                      String ABL,
                      String titleShort,
                      String titleLong,
                      String dateStart,
                      String dateEnd,
                      String status,
                      String tags,
                      String assocLead,
                      String assocElec,
                      String assocMech,
                      long progress,
                      String customer,
                      String notes
    ) {
        this.projectID = id;
        this.projectABL = ABL;
        this.projectTitleShort = titleShort;
        this.projectTitleLong = titleLong;
        this.projectDateStart = dateStart;
        this.projectDateEnd = dateEnd;
        this.projectStatus = status;
        this.projectTags = tags;
        this.projectAssocLead = assocLead;
        this.projectAssocElec = assocElec;
        this.projectAssocMech = assocMech;
        this.projectProgress = progress;
        this.projectCustomer = customer;
        this.projectNotes = notes;
    }



    // #SET/#GET      Properties of the Class        HERE
    public void setID(int id) {this.projectID = id;}
    public int getID() {return this.projectID;}
    public void setProjectABL(String abl) {this.projectABL = abl;}
    public String getProjectABL() {return this.projectABL;}
    public void setProjectShort(String titleShort) {this.projectTitleShort = titleShort;}
    public String getProjectShort() {return this.projectTitleShort;}
    public void setProjectLong(String titleLong) {this.projectTitleLong = titleLong;}
    public String getProjectLong() {return this.projectTitleLong;}
    public void setProjectDateStart(String body) {this.projectDateStart = body;}
    public String getProjectDateStart() {return this.projectDateStart;}
    public void setProjectDateEnd(String datetime) {this.projectDateEnd = datetime;}
    public String getProjectDateEnd() {return this.projectDateEnd;}
    public void setProjectStatus(String status) {this.projectStatus = status;}
    public String getProjectStatus() {return this.projectStatus;}
    public void setProjectTags(String tags) {this.projectTags = tags;}
    public String getProjectTags() {return this.projectTags;}
    public void setProjectAssocLead(String assignee) {this.projectAssocLead = assignee;}
    public String getProjectAssocLead() {return this.projectAssocLead;}
    public void setProjectAssocElec(String project) {this.projectAssocElec = project;}
    public String getProjectAssocElec() {return this.projectAssocElec;}
    public void setProjectAssocMech(String milestone) {this.projectAssocMech = milestone;}
    public String getProjectAssocMech() {return this.projectAssocMech;}
    public void setProjectProgress(long progress) {this.projectProgress = progress;}
    public long getProjectProgress() {return this.projectProgress;}
    public void setProjectCustomer(String ticket) {this.projectCustomer = ticket;}
    public String getProjectCustomer() {return this.projectCustomer;}
    public void setProjectNotes(String owner) {this.projectNotes = owner;}
    public String getProjectNotes() {return this.projectNotes;}


    public String getColumnValue(String column) {
        String outputString = "";

        switch (column){
            case (Contract_Project.ProjectEntry._ID): outputString = String.valueOf(getID()); break;
            case (Contract_Project.ProjectEntry.COLUMN_ABL): outputString = getProjectABL(); break;
            case (Contract_Project.ProjectEntry.COLUMN_TITLE_SHORT): outputString = getProjectShort(); break;
            case (Contract_Project.ProjectEntry.COLUMN_TITLE_LONG): outputString = getProjectLong(); break;
            case (Contract_Project.ProjectEntry.COLUMN_DATESTART): outputString = getProjectDateStart(); break;
            case (Contract_Project.ProjectEntry.COLUMN_DATEEND): outputString = getProjectDateEnd(); break;
            case (Contract_Project.ProjectEntry.COLUMN_STATUS): outputString = getProjectStatus(); break;
            case (Contract_Project.ProjectEntry.COLUMN_TAGS): outputString = getProjectTags(); break;
            case (Contract_Project.ProjectEntry.COLUMN_LEAD): outputString = getProjectAssocLead(); break;
            case (Contract_Project.ProjectEntry.COLUMN_ELEC): outputString = getProjectAssocElec(); break;
            case (Contract_Project.ProjectEntry.COLUMN_MECH): outputString = getProjectAssocMech(); break;
            case (Contract_Project.ProjectEntry.COLUMN_PROGRESS): outputString = String.valueOf(getProjectProgress()); break;
            case (Contract_Project.ProjectEntry.COLUMN_CUSTOMER): outputString = getProjectCustomer(); break;
            case (Contract_Project.ProjectEntry.COLUMN_NOTES): outputString = getProjectNotes(); break;
        }

        return outputString;
    }
}
