package com.tnk.pha.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tnk.R;
import com.tnk.db.items.Item_Project;

import java.util.List;

public class ProjectsGridAdapter extends BaseAdapter {

    private final Context tContext;
    private final List<Item_Project> projects;
    private LayoutInflater layoutInflater;

    public ProjectsGridAdapter(Context newContext, List<Item_Project> newProjects){
        this.tContext = newContext;
        this.projects = newProjects;
        layoutInflater = LayoutInflater.from(tContext);



    }

    @Override
    public int getCount(){
        return projects.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return projects.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = layoutInflater.inflate(R.layout.grid_item_project, null);
            holder.ablView = (TextView) convertView.findViewById(R.id.gitem_projectABL);
            holder.shortView = (TextView) convertView.findViewById(R.id.gitem_projectShort);
            holder.progressView = (TextView) convertView.findViewById(R.id.gitem_projectProgress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Item_Project project = this.projects.get(position);
        holder.ablView.setText(project.getProjectABL());
        holder.shortView.setText(project.getProjectShort());
        holder.progressView.setText(String.valueOf(project.getProjectProgress()));

        int prog = Math.toIntExact(project.getProjectProgress());

        if (0 <= prog && prog <= 33) {
            holder.progressView.setBackgroundColor(Color.RED);
        } else if (33 < prog && prog <= 66) {
            holder.progressView.setBackgroundColor(Color.YELLOW);
        } else if (prog > 66) {
            holder.progressView.setBackgroundColor(Color.GREEN);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView ablView;
        TextView shortView;
        TextView progressView;
    }
}
