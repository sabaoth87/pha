package com.tnk.pha;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;

import com.tnk.R;
import com.tnk.pha.adapters.PHA_ELA_MealPlanner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PHA_MealPlanner extends AppCompatActivity {

    private static String TAG = "PHA[MP]";
    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    //public final Spinner sp_MP_main;
    //public Spinner sp_MP_recipe;
    //public Spinner sp_MP_servings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pha_mealplanner);


        Button btn_MP_add = findViewById(R.id.btn_mp_add);
        Button btn_MP_clear = findViewById(R.id.btn_mp_clear);

        final Spinner sp_MP_main = findViewById(R.id.sp_main_ingred);
        final Spinner sp_MP_recipe = findViewById(R.id.sp_recipe);
        final Spinner sp_MP_servings = findViewById(R.id.sp_servings);

        ExpandableListView expListView = findViewById(R.id.elv_mp);

        loadELVdata();

        //A list of heads and a list of children needs to be build from the SQL Db

        listAdapter = new PHA_ELA_MealPlanner(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        //et_MP_name = findViewById(R.id.et_edit_tool_name);
        //et_MP_quantity = findViewById(R.id.et_edit_tool_quantity);

        btn_MP_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Log.v(TAG, "ATTEMPT: Add Tool");
                //addEntry();
            }
        });

        btn_MP_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "ATTEMPT: Clear Fields!");
                //clearInputs();
            }
        });


        //                          FOR MY SPINNERS
        //Create and ArrayAdapter using the string array and default layout
        final ArrayAdapter<CharSequence> adap_mi = ArrayAdapter.createFromResource(this,
                R.array.ingredients, android.R.layout.simple_spinner_item);
        final ArrayAdapter<CharSequence> adap_mi_chicken = ArrayAdapter.createFromResource(this,
                R.array.ingredients_chicken, android.R.layout.simple_spinner_item);
        final ArrayAdapter<CharSequence> adap_mi_pork = ArrayAdapter.createFromResource(this,
                R.array.ingredients_pork, android.R.layout.simple_spinner_item);
        final ArrayAdapter<CharSequence> adap_mi_fish = ArrayAdapter.createFromResource(this,
                R.array.ingredients_fish, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adap_servings = ArrayAdapter.createFromResource(this,
                R.array.servings, android.R.layout.simple_spinner_item);
        //Specify the layout to use when the list of choices appears
        adap_mi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adap_mi_chicken.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adap_mi_fish.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adap_mi_pork.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adap_servings.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Apply the adapter to the spinner
        sp_MP_main.setAdapter(adap_mi);
        sp_MP_recipe.setAdapter(adap_mi_chicken);
        sp_MP_servings.setAdapter(adap_servings);


        sp_MP_main.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String spinnerValue = sp_MP_main.getSelectedItem().toString();

                if (spinnerValue.equals("Chicken")) {
                    sp_MP_recipe.setAdapter(adap_mi_chicken);
                }else if (spinnerValue.equals("Pork")){
                    sp_MP_recipe.setAdapter(adap_mi_pork);
                }else if (spinnerValue.equals("Fish")){
                    sp_MP_recipe.setAdapter(adap_mi_fish);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void loadELVdata() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listDataHeader.add("Monday");
        listDataHeader.add("Tuesday");
        listDataHeader.add("Wednesday");
        listDataHeader.add("Thursday");
        listDataHeader.add("Friday");
        listDataHeader.add("Saturday");
        listDataHeader.add("Sunday");

        List<String> mp_monday = new ArrayList<String>();
        mp_monday.add("monday null");

        List<String> mp_tuesday = new ArrayList<String>();
        mp_tuesday.add("tuesday null");

        List<String> mp_wednesday = new ArrayList<String>();
        mp_wednesday.add("wednesday null");

        List<String> mp_thursday = new ArrayList<String>();
        mp_thursday.add("thursday null");

        List<String> mp_friday = new ArrayList<String>();
        mp_friday.add("friday null");

        List<String> mp_saturday = new ArrayList<String>();
        mp_saturday.add("saturday null");

        List<String> mp_sunday = new ArrayList<String>();
        mp_sunday.add("sunday null");

        listDataChild.put(listDataHeader.get(0), mp_monday);
        listDataChild.put(listDataHeader.get(1), mp_tuesday);
        listDataChild.put(listDataHeader.get(2), mp_wednesday);
        listDataChild.put(listDataHeader.get(3), mp_thursday);
        listDataChild.put(listDataHeader.get(4), mp_friday);
        listDataChild.put(listDataHeader.get(5), mp_saturday);
        listDataChild.put(listDataHeader.get(6), mp_sunday);


    }
}
