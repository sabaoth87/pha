package com.tnk.db;

import android.provider.BaseColumns;
import android.support.annotation.IntDef;

public interface dbConstants extends BaseColumns {
	
	public static final String DATABASE_NAME= "PHAY_DbVersion_alpha_1";
	public static final String TABLE_NAME="MemosDb";
	// Columns in the 'memos'  db
	public static final String TITLE = "title" ;
	public static final String DATE = "date";
	public static final String AUTHOR = "author" ;
	public static final String MEMO = "memo" ;
	public static final String COMMENT = "comment" ;
	public static final String TIME = "time";
	public static final String CATEGORY = "category";
	public static final String TAGS = "tags";
	
	
	//vdb Strings
	public static final String[] vdbCommands1 = { 	"open", 
													"find", 
													"what", 
													"where",
													"when",
													"search",
													"time",
													"date",
												};
	public static final String[] vdbDateKWs = {
		"January","january","February", "february","March", "march",
		"April","april","May","may","June","june","July","july",
		"August","august","September","september","October","october",
		"November","november","December","december",
		"Monday","monday","Tuesday","tuesday","Wednesday","wednesday",
		"Thursday","thursday","Friday","friday","Saturday","saturday", "Sunday","sunday"};
	
	public static final String[] vdbDateObscure = {
		"next","tomorrow","tonight", "this"
	};
	
	public static final String[] vdbDateObscure1 = {
		"week", "month", "morning","afternoon", "evening"
	};

	/*
	Constants for Projects
	 */
	int FINDBY_ID = -1;
	int FINDBY_ABL = 0;
	int FINDBY_SHORT = 1;
	int FINDBY_LONG = 2;
	int FINDBY_DATESTART = 3;
	int FINDBY_DATEEND = 4;
	int FINDBY_STATUS = 5;
	int FINDBY_TAGS = 6;
	int FINDBY_LEAD = 7;
	int FINDBY_ELEC = 8;
	int FINDBY_MECH = 9;
	int FINDBY_PROGRESS = 10;
	int FINDBY_CUSTOMER = 11;

	int FINDALL = 87;

}