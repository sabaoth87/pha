package com.tnk.pha.project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.tnk.R;
import com.tnk.db.contracts.Contract_Project;
import com.tnk.db.dbConstants;
import com.tnk.db.helpers.DbHelper_Projects;
import com.tnk.db.items.Item_Project;
import com.tnk.pha.util.Variables;

public class PHA_Project_View extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "PHA_Projects View :: ";

    /*
    Shared Vars
     */
    private Bundle extras;
    private boolean incomingProject = false;
    private boolean beginEntrySet = false;
    private static final int ACTIVITY_CREATE = 0, ACTIVITY_EDIT = 1;
    static final String DATE_TIME_FORMAT = "yyyy-MM-dd kk:mm:ss";
    private Long RowId;
    private String RowIdString;
    private DbHelper_Projects PHA_dbHelper;
    private String newString;

    /*
    Shared Vars
    Module-Specific Naming
     */
    private Toolbar toolbar;

    /*
    Module Specific Vars
     */
    private TextView tv_abl, tv_short, tv_long, tv_datestart, tv_dateend, tv_status,
            tv_tags, tv_lead, tv_elec, tv_mech, tv_progress, tv_customer, tv_notes;
    private Item_Project currentProject;
    private ProgressBar pb_project, pb_time;
    private FloatingActionButton fab_view;
    private Animation animSecondaryRotate;
    private Animation animPBFlip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_view);
        toolbar = findViewById(R.id.tb_project_view);
        setSupportActionBar(toolbar);

        RowId = savedInstanceState != null ? savedInstanceState.getLong(Contract_Project.ProjectEntry._ID) : null;
        PHA_dbHelper = new DbHelper_Projects(this);

        tv_abl = findViewById(R.id.tv_project_view_abl);
        tv_short = findViewById(R.id.tv_project_view_short);
        tv_long = findViewById(R.id.tv_project_view_long);
        tv_datestart = findViewById(R.id.tv_project_view_date_start);
        tv_dateend = findViewById(R.id.tv_project_view_date_end);
        tv_status = findViewById(R.id.tv_project_view_status);
        // TODO Implement in the future!!
        //tv_tags = findViewById(R.id.tv_project_view_tags);
        tv_lead = findViewById(R.id.tv_project_view_lead);
        tv_elec =findViewById(R.id.tv_project_view_elec);
        tv_mech = findViewById(R.id.tv_project_view_mech);
        tv_progress = findViewById(R.id.tv_project_view_progress);
        tv_customer = findViewById(R.id.tv_project_view_customer);
        tv_notes = findViewById(R.id.tv_project_view_notes);

        pb_project = (ProgressBar) findViewById(R.id.pb_project_view_progressbar);
        pb_time = (ProgressBar) findViewById(R.id.pb_project_view_time_progressbar);

        fab_view = (FloatingActionButton) findViewById(R.id.fab_project_view);
        fab_view.bringToFront();
        fab_view.setOnClickListener(this);
/*
        FloatingActionButton fab_project_view = (FloatingActionButton) findViewById(R.id.fab_project_view);
        fab_project_view.bringToFront();
        fab_project_view.setOnClickListener(this::onClick);
*/
        animSecondaryRotate = AnimationUtils.loadAnimation(
                getApplicationContext(),
                R.anim.rotate_secondary_progress);
        animPBFlip = AnimationUtils.loadAnimation(
                getApplicationContext(),
                R.anim.spin_middle
        );

        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                /* Not too sure what to add here for Projects yet...*/
            } else {
                Log.v(TAG, "Setting intent values");
                // TODO Investigate this in PHA_Reminders for implementation
                //findEntryForEdit(extras.getString(Contract_Project.ProjectEntry._ID));
                incomingProject = true;
                Cursor cProject = findEntryForEdit(extras.getString(Contract_Project.ProjectEntry._ID));
                if (cProject != null) {
                    Log.v(TAG, "<Success> Db entry location valid! /n Returning information");
                    loadProjectValues(cProject);
                }
                Log.v(TAG, "<Error> Issue Cursor was invalid!");
            }
        } else {
            Log.v(TAG, "Setting default values");
            newString = (String) savedInstanceState.getSerializable("want");

            // TODO Fill this will default value population!

        }

        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            int rowId = extras != null ? extras.getInt("RowId") : -1;
            //Do things with the RowID here
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        PHA_dbHelper.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        extras = getIntent().getExtras();
        if (extras == null) {
            /* Not too sure what to add here for Projects yet...*/
        } else {
            Log.v(TAG, "Setting intent values");
            // TODO Investigate this in PHA_Reminders for implementation
            //findEntryForEdit(extras.getString(Contract_Project.ProjectEntry._ID));
            incomingProject = true;
            Cursor cProject = findEntryForEdit(extras.getString(Contract_Project.ProjectEntry._ID));
            if (cProject != null) {
                Log.v(TAG, "<Success> Db entry location valid! Returning information");
                loadProjectValues(cProject);
            } else {
                Log.v(TAG, "<Error> Issue Cursor was invalid!");
                Snackbar.make(this.getCurrentFocus(), "ERROR#0000There was a problem with that entry...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
        populateFields();
    }

    private void populateFields() {

        if (RowId != null) {

        } else if (incomingProject) {
            /*
            This is the area to populate fields that is generated from the Intent package
             */
            tv_abl.setText(newString);
        } else {

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String defaultABLKey = "deafault_project_abl";
            String defaultABL = prefs.getString(defaultABLKey, "");
            // TODO Establish the preferences...
            if ("".equals(defaultABL) == false)
                tv_abl.setText(defaultABL);

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
         /*
         @FIX_ME __ - onSaveInstanceState
         Attempting to stash the RowId of the current RowId, so we can try to persist
          */
        outState.putLong(Contract_Project.ProjectEntry._ID, RowId);
    }

    public void loadProjectValues(Cursor project) {
        //load the Project information into the UI here
        //fab_view.setEnabled(false);

        if (Variables.mode_debug){Log.v(TAG,"Trying to load Editable Project...");}
        if (project==null) {
            if (Variables.mode_debug){Log.v(TAG,"Cursor with the Editable Project was found to be null!");}
            tv_abl.setText(R.string.et_entry_null);
            tv_short.setText(R.string.et_entry_null_body);
            tv_long.setText(R.string.et_entry_null_body);

            // TODO INVESTIGATE beginEntrySet
            beginEntrySet = false;
        }
        else {
            if (Variables.mode_debug){Log.v(TAG,"Cursor is valid! Populating UI");}

            while(project.moveToNext()) {
                int index;
                // how many columns to cycle through?
                int columnsLength = Contract_Project.ProjectEntry.COLUMNS.length;
                String[] outputString = new String[columnsLength];

                int count = 0;

                for (String s: Contract_Project.ProjectEntry.COLUMNS) {

                    index  = project.getColumnIndexOrThrow(s);
                    outputString[count] = project.getString(index);
                    count++;

                }
                index = project.getColumnIndexOrThrow(Contract_Project.ProjectEntry._ID);
                long id = project.getLong(index);
                RowIdString = outputString[Contract_Project.ProjectEntry.INT_ID];
                RowId = id;

                if(Variables.mode_debug)Log.v(TAG, "Editable Reminder entry ID: " + outputString[Contract_Project.ProjectEntry.INT_ID]);

                tv_abl.setText(outputString[Contract_Project.ProjectEntry.INT_ABL]);
                tv_short.setText(outputString[Contract_Project.ProjectEntry.INT_TITLE_SHORT]);
                tv_long.setText(outputString[Contract_Project.ProjectEntry.INT_TITLE_LONG]);
                tv_datestart.setText(outputString[Contract_Project.ProjectEntry.INT_DATESTART]);
                tv_dateend.setText(outputString[Contract_Project.ProjectEntry.INT_DATEEND]);
                tv_status.setText(outputString[Contract_Project.ProjectEntry.INT_STATUS]);
                //tv_tags.setText(outputString[Contract_Project.ProjectEntry.INT_TAGS]);
                tv_lead.setText(outputString[Contract_Project.ProjectEntry.INT_LEAD]);
                tv_elec.setText(outputString[Contract_Project.ProjectEntry.INT_ELEC]);
                tv_mech.setText(outputString[Contract_Project.ProjectEntry.INT_MECH]);
                tv_progress.setText(outputString[Contract_Project.ProjectEntry.INT_PROGRESS]);
                tv_customer.setText(outputString[Contract_Project.ProjectEntry.INT_CUSTOMER]);
                tv_notes.setText(outputString[Contract_Project.ProjectEntry.INT_NOTES]);

                tv_abl.setEnabled(false);
                tv_short.setEnabled(false);
                tv_long.setEnabled(false);
                tv_datestart.setEnabled(false);
                tv_dateend.setEnabled(false);
                tv_status.setEnabled(false);
                //tv_tags.setEnabled(false);
                tv_lead.setEnabled(false);
                tv_elec.setEnabled(false);
                tv_mech.setEnabled(false);
                tv_progress.setEnabled(false);
                tv_customer.setEnabled(false);
                tv_notes.setEnabled(false);

                int progress = Math.toIntExact(Integer.valueOf(outputString[Contract_Project.ProjectEntry.INT_PROGRESS]));

                pb_project.setMax(100);
                //pb_project.setMin(0);
                pb_project.setProgress(0);
                pb_project.setIndeterminate(false);
                pb_project.setProgress(progress);
                pb_project.setSecondaryProgress(progress);
                beginEntrySet = true;
                pb_time.setMax(100);
                //pb_time.setMin(0);
                pb_time.setProgress(Math.toIntExact(Integer.valueOf(outputString[Contract_Project.ProjectEntry.INT_PROGRESS])));

                pb_project.startAnimation(animSecondaryRotate);
                //pb_project.startAnimation(animPBFlip);
                //pb_project.
            }

            beginEntrySet = false;
        }
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {

            /*
            TODO - Populate the Toolbar
             */
            case (R.id.fab_project_view):
                Context context = getApplicationContext();
                setResult(RESULT_OK);


                Snackbar.make(arg0, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                //
                Intent editIntent = new Intent(context, PHA_Projects_Entry.class);
                editIntent.putExtra(Contract_Project.ProjectEntry._ID, RowIdString);
                startActivityForResult(editIntent, ACTIVITY_EDIT);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Create a menu for Projects
        getMenuInflater().inflate(R.menu.tb_menu_prj_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_item_prj_add){
            Context context = getApplicationContext();

            Intent editIntent = new Intent(context, PHA_Projects_Entry.class);
            editIntent.putExtra(Contract_Project.ProjectEntry._ID, RowId);
            startActivityForResult(editIntent, ACTIVITY_EDIT);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Cursor findEntryForEdit(String id) {
        Context context = getApplicationContext();
        DbHelper_Projects dbHelper = new DbHelper_Projects(context);

        Cursor forEdit = dbHelper.findProject(dbConstants.FINDBY_ID, id);
        return forEdit;
    }

}
