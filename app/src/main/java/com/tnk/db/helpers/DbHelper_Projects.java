package com.tnk.db.helpers;

import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tnk.db.contracts.Contract_Project;
import com.tnk.db.dbConstants;
import com.tnk.db.items.Item_Project;
import com.tnk.pha.util.FileOps;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class DbHelper_Projects extends SQLiteOpenHelper {

    public static final String TAG = "DbHelper[Projects] ::";

    public static String DB_FILEPATH = "/data/data/com.tnk.pha/databases/WorkBenchTools.db";

    //information of database
    private static final int DATABASE_VERSION = 0001;
    private static final String DATABASE_NAME = "PHA.db";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Contract_Project.ProjectEntry.TABLE_NAME + " (" +
                    Contract_Project.ProjectEntry._ID + " INTEGER PRIMARY KEY," +
                    Contract_Project.ProjectEntry.COLUMN_ABL + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_TITLE_SHORT + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_TITLE_LONG + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_DATESTART + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_DATEEND + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_STATUS + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_TAGS + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_LEAD + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_ELEC + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_MECH + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_PROGRESS + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_CUSTOMER + " TEXT," +
                    Contract_Project.ProjectEntry.COLUMN_NOTES + " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Contract_Project.ProjectEntry.TABLE_NAME;

    public DbHelper_Projects(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {

        Log.v(TAG, "<<>> CREATING DB <<>> " + DATABASE_NAME + " " + Contract_Project.ProjectEntry.TABLE_NAME + " v- " + DATABASE_VERSION);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVerseion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply discard the data and start over
        Log.v(TAG, ":: Attempting to upgrade the Db");

        Log.v(TAG, "<<>> DELETING THE CURRENT DB <<>> " + DATABASE_NAME + " " + Contract_Project.ProjectEntry.TABLE_NAME + " v- " + DATABASE_VERSION);
        db.execSQL(SQL_DELETE_ENTRIES);
        Log.v(TAG, "<<>> Creating a NEW DB at version '" + DATABASE_VERSION + "' <<>>");
        //db.execSQL(SQL_CREATE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long addProject(Item_Project newProject) {
        Log.v(TAG, "Opening the local Db for writing...");
        SQLiteDatabase db = this.getWritableDatabase();
        Log.v(TAG, "Db Open!");

        /*
        Populate an set of values to insert into the database
         */
        ContentValues values = new ContentValues();
        values.put(Contract_Project.ProjectEntry.COLUMN_ABL, newProject.getProjectABL());
        values.put(Contract_Project.ProjectEntry.COLUMN_TITLE_SHORT, newProject.getProjectShort());
        values.put(Contract_Project.ProjectEntry.COLUMN_TITLE_LONG, newProject.getProjectLong());
        values.put(Contract_Project.ProjectEntry.COLUMN_DATESTART, newProject.getProjectDateStart());
        values.put(Contract_Project.ProjectEntry.COLUMN_DATEEND, newProject.getProjectDateEnd());
        values.put(Contract_Project.ProjectEntry.COLUMN_STATUS, newProject.getProjectStatus());
        values.put(Contract_Project.ProjectEntry.COLUMN_TAGS, newProject.getProjectTags());
        values.put(Contract_Project.ProjectEntry.COLUMN_LEAD, newProject.getProjectAssocLead());
        values.put(Contract_Project.ProjectEntry.COLUMN_ELEC, newProject.getProjectAssocElec());
        values.put(Contract_Project.ProjectEntry.COLUMN_MECH, newProject.getProjectAssocMech());
        values.put(Contract_Project.ProjectEntry.COLUMN_PROGRESS, newProject.getProjectProgress());
        values.put(Contract_Project.ProjectEntry.COLUMN_CUSTOMER, newProject.getProjectCustomer());
        values.put(Contract_Project.ProjectEntry.COLUMN_NOTES, newProject.getProjectNotes());

        Log.d(TAG, "addProject: Adding entry to the Db...");
        long newRowId = db.insert(
                Contract_Project.ProjectEntry.TABLE_NAME,
                null,
                values);
        Log.v(TAG, "Entry " + newRowId + " added to Db");
        Log.d(TAG, "addProject: Entry " + newRowId + " added to Db.");
        return (newRowId);
    }

    public boolean updateProjectById(Item_Project targetProject) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        String[] outputStrings = new String[Contract_Project.ProjectEntry.COLUMNS.length];
        String index = "";
        int count = 0;

        for (String s: Contract_Project.ProjectEntry.COLUMNS) {
            index  = targetProject.getColumnValue(s);
            Log.v(TAG, "Updating " + s+ " with " + index +" value");
            values.put(s,index);
            count++;

        }

        String selection = Contract_Project.ProjectEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(targetProject.getID()) };

        int response = db.update(
                Contract_Project.ProjectEntry.TABLE_NAME,
                values,
                "_id="+targetProject.getID(),
                null);

        if (response>0) {
            Log.v(TAG, "Entry Updated?" + response + " from "+targetProject.getID());
            return TRUE;
        } else {
            Log.v(TAG, "Entry NOT Updated?" + response+ " from "+targetProject.getID());

            return FALSE;
        }
    }


    public boolean deleteProjectById(String toolId){
        SQLiteDatabase db = getWritableDatabase();

        //Define 'where' part of the query.
        String selection = Contract_Project.ProjectEntry._ID + " LIKE ?";
        //Specify arguments in placeholder order.
        String[] selectionArgs = { toolId };
        //Issue the SQL statement.
        db.delete(Contract_Project.ProjectEntry.TABLE_NAME, selection, selectionArgs);
        return TRUE;
    }

    public Cursor findProject(int findMethod, String findTarget) {

        Cursor returningCursor = null;
        SQLiteDatabase readDb = this.getReadableDatabase();

        switch (findMethod) {
            case dbConstants.FINDBY_ABL:

                Log.d(TAG, "findProject: Searching the local Db for " + findTarget);

                //Define a projection that specifies which columns from the database
                //you will actually use after this query
                String[] projection = {
                        Contract_Project.ProjectEntry._ID,
                        Contract_Project.ProjectEntry.COLUMN_ABL,
                        Contract_Project.ProjectEntry.COLUMN_TITLE_SHORT,
                        Contract_Project.ProjectEntry.COLUMN_TITLE_LONG,
                        Contract_Project.ProjectEntry.COLUMN_DATESTART,
                        Contract_Project.ProjectEntry.COLUMN_DATEEND,
                        Contract_Project.ProjectEntry.COLUMN_STATUS,
                        Contract_Project.ProjectEntry.COLUMN_TAGS,
                        Contract_Project.ProjectEntry.COLUMN_LEAD,
                        Contract_Project.ProjectEntry.COLUMN_ELEC,
                        Contract_Project.ProjectEntry.COLUMN_MECH,
                        Contract_Project.ProjectEntry.COLUMN_PROGRESS,
                        Contract_Project.ProjectEntry.COLUMN_CUSTOMER,
                        Contract_Project.ProjectEntry.COLUMN_NOTES
                };

                // Filter results where the 'ABL' is = 'findTarget'
                String selection = Contract_Project.ProjectEntry.COLUMN_ABL + " ?";
                String[] selectionArgs = {findTarget};

                // How you want the results sorted in the resulting Cursor
                String sortOrder =
                        Contract_Project.ProjectEntry._ID + " DESC";

                returningCursor = readDb.query(
                        Contract_Project.ProjectEntry.TABLE_NAME,       // the table to query
                        projection,                                     // the columns to return
                        selection,                                      // the columns for the WHERE clause
                        selectionArgs,                                  // the values for the WHERE clause
                        null,                                   // do/do not group the rows
                        null,                                    // do/do not filter by row group
                        sortOrder                                       // the sort order
                );

                break;

            case dbConstants.FINDBY_ID:

                Log.d(TAG, "findProject: Searching for a Project entry by _ID");

                String query =
                        "SELECT * FROM " +
                                Contract_Project.ProjectEntry.TABLE_NAME +
                                " WHERE _id = " +
                                findTarget;
                returningCursor = readDb.rawQuery(query, null);
                break;

            case dbConstants.FINDALL:
                Log.d(TAG, "findProject: Searching for ALL Projects");

                returningCursor = readDb.rawQuery(
                        "select * from " +
                                Contract_Project.ProjectEntry.TABLE_NAME,
                        null);
                break;
        }
        return returningCursor;
    }

    public boolean checkTable(SQLiteDatabase db) {

        if(db == null || !db.isOpen()){
            db = getReadableDatabase();
        }

        if(!db.isReadOnly()) {
            db.close();
            db = getReadableDatabase();
        }

        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" +
                Contract_Project.ProjectEntry.TABLE_NAME + "'", null);

        if (cursor!=null) {
            if (cursor.getCount() > 0) {
                Log.v(TAG, "Table was found!");
                cursor.close();
                return true;
            }
            Log.v(TAG, "Table was not found  ¯_(ツ)_¯");
            cursor.close();
        }
        return false;
    }

    /**
     * Copies the database file at the specified location over the current
     * internal application database.
     */
    public boolean importDatabase(String dbPath) throws IOException {

        // Close the SQLiteOpenHelper so it will commit the created empty
        // database to internal storage
        close();
        File newDb = new File(dbPath);
        File oldDb = new File(DB_FILEPATH);
        if (newDb.exists()) {
            FileOps.copyFile(new FileInputStream(newDb), new FileInputStream(oldDb));
            // Access the copied database so SQLiteHelper will cache it and mark
            // it as created.
            getWritableDatabase().close();
            return true;
        }
        return false;
    }

    public boolean exportDatabase(String exportPath) throws IOException {

        // Close the SQLiteOpenHelper so it will commit the created empty
        // database to internal storage
        close();
        File destination = new File(exportPath);
        File dbPath = new File(DB_FILEPATH);
        if (dbPath.exists()) {
            FileOps.copyFile(new FileInputStream(destination), new FileInputStream(dbPath));
            // Access the copied database so SQLiteHelper will cache it and mark
            // it as created.
            getWritableDatabase().close();
            return true;
        }
        return false;
    }

    public boolean create_table (Context context) {
        DbHelper_Projects dbHelper_projects = new DbHelper_Projects(context);
        SQLiteDatabase db = dbHelper_projects.getWritableDatabase();
        db.execSQL(SQL_CREATE_ENTRIES);
        return true;
    }
}
